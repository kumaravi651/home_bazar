import 'package:flutter/material.dart';
import 'package:home_bazar/dependencies_provider.dart' as di;
import 'package:home_bazar/src/common/bloc/cart_bloc.dart';
import 'package:home_bazar/src/presentation/home/home_page.dart';
import 'package:home_bazar/src/presentation/login/login_page.dart';
import 'package:home_bazar/src/presentation/product_details/product_detail.dart';
import 'dependencies_provider.dart';
import 'src/common/bloc/bloc_provider.dart';

void main() {
  di.init();

  runApp(App());
}

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      bloc: getIt<CartBloc>(),
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Shopping Cart Flutter',
        theme: ThemeData(
          primarySwatch: Colors.blue,
          canvasColor: Colors.grey[50],
        ),
        home: LoginPage(),
      ),
    );
  }
}
