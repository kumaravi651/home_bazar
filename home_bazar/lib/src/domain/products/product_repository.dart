import 'package:home_bazar/src/domain/products/product.dart';

abstract class ProductRepository {
  Future<List<Product>> get();
}
