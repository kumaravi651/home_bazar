import 'package:email_validator/email_validator.dart';
import 'package:flutter/material.dart';
import 'package:home_bazar/src/presentation/home/home_page.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final formKey = GlobalKey<FormState>();
  final scaffoldKey = GlobalKey<ScaffoldState>();
  TextEditingController textEmailController = TextEditingController();

  String _email;
  String _password;

  void _submitCommand() {
    final form = formKey.currentState;

    if (form.validate()) {
      form.save();
      _loginCommand();
    }
  }

  void _loginCommand() {
    Navigator.of(context)
        .push(MaterialPageRoute(builder: (context) => HomePage()));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scaffoldKey,
      appBar: AppBar(
        title: const Text('Email validation example'),
      ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Form(
          key: formKey,
          child: Column(
            children: [
              TextFormField(
                key: const Key('EmailField'),
                controller: textEmailController,
                decoration: const InputDecoration(labelText: 'Email'),
                validator: (val) => !EmailValidator.validate(val, true)
                    ? 'Not a valid email.'
                    : null,
                onSaved: (val) => _email = val,
              ),
              TextFormField(
                key: const Key('PasswordField'),
                decoration: const InputDecoration(labelText: 'Password'),
                validator: (val) =>
                    val.length < 8 ? 'Password too short..' : null,
                onSaved: (val) => _password = val,
                obscureText: true,
              ),
              RaisedButton(
                key: const Key('SignIn'),
                onPressed: _submitCommand,
                child: const Text('Sign in'),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
