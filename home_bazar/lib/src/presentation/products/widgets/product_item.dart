import 'package:flutter/material.dart';
import 'package:home_bazar/src/common/bloc/bloc_provider.dart';
import 'package:home_bazar/src/common/bloc/cart_bloc.dart';
import 'package:home_bazar/src/common/bloc/products_state.dart';

class ProductItem extends StatelessWidget {
  final ProductItemState _productItem;

  const ProductItem(this._productItem);

  @override
  Widget build(BuildContext context) {
    final cartBloc = BlocProvider.of<CartBloc>(context);

    return Card(
        child: Column(
      children: <Widget>[
        Expanded(
          flex: 5,
          child: Image.network(
            _productItem.image,
            fit: BoxFit.fitWidth,
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text(
            _productItem.title,
            maxLines: 2,
            overflow: TextOverflow.ellipsis,
            style: Theme.of(context).textTheme.bodyText1,
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text(
            _productItem.price,
            maxLines: 2,
            overflow: TextOverflow.ellipsis,
            style: Theme.of(context).textTheme.bodyText1,
          ),
        ),
        RawMaterialButton(
          child: Text(
            'Add to cart'.toUpperCase(),
            style: Theme.of(context)
                .textTheme
                .button
                .copyWith(color: Theme.of(context).primaryColor),
          ),
          onPressed: () => cartBloc.addProductToCartCart(_productItem),
        )
      ],
    ));
  }
}
