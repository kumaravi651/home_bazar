import 'package:flutter/material.dart';
import 'package:home_bazar/src/presentation/cart/widgets/cart_counter_badge.dart';

class MyAppBar extends StatelessWidget implements PreferredSizeWidget {
  MyAppBar() : preferredSize = const Size.fromHeight(kToolbarHeight);

  @override
  final Size preferredSize;

  @override
  Widget build(BuildContext context) {
    return AppBar(
      title: const Text('Shopping'),
      actions: <Widget>[shoppingCartIcon(context)],
    );
  }

  Widget shoppingCartIcon(BuildContext context) {
    // Using Stack to show Notification Badge
    return Stack(
      children: <Widget>[
        IconButton(
          icon: const Icon(Icons.shopping_cart),
          onPressed: () {
            Scaffold.of(context).openEndDrawer();
          },
        ),
        CartCounterBadge()
      ],
    );
  }
}
