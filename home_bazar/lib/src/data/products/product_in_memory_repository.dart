import 'dart:convert';

import 'package:home_bazar/src/domain/products/product.dart';
import 'package:home_bazar/src/domain/products/product_repository.dart';

const products = '''[
  {
    "id": "1",
    "image":
    "https://m.media-amazon.com/images/I/81giLCXfxIL._AC_UL640_FMwebp_QL65_.jpg",
    "title": "STATUS MANTRA",
    "price": 1999.95
  },
  {
    "id": "2",
    "image":
    "https://m.media-amazon.com/images/I/81giLCXfxIL._AC_UL640_FMwebp_QL65_.jpg",
    "title": "Tee Shirt",
    "price": 2122.95
  },
  {
    "id": "3",
    "image":
    "https://m.media-amazon.com/images/I/81ZYZ9yl1hL._AC_UL640_FMwebp_QL65_.jpg",
    "title": "Element Skater",
    "price": 521.45
  },
  {
    "id": "4",
    "image":
    "https://m.media-amazon.com/images/I/61-DwEh1zrL._AC_UL640_FMwebp_QL65_.jpg",
    "title": "Element Logo",
    "price": 458.90
  },
  {
    "id": "5",
    "image":
    "https://m.media-amazon.com/images/I/81giLCXfxIL._AC_UL640_FMwebp_QL65_.jpg",
    "title": "Element Latest",
    "price": 727.95
  },
  {
    "id": "6",
    "image":
    "https://m.media-amazon.com/images/I/81giLCXfxIL._AC_UL640_FMwebp_QL65_.jpg",
    "title": "Element Combo",
    "price": 913.90
  },
  {
    "id": "7",
    "image":
    "https://m.media-amazon.com/images/I/81giLCXfxIL._AC_UL640_FMwebp_QL65_.jpg",
    "title": "Element Shirt",
    "price": 619.95
  },
  {
    "id": "8",
    "image":
    "https://m.media-amazon.com/images/I/7119OAEE+gL._AC_UL640_FMwebp_QL65_.jpg",
    "title": "Element Tones",
    "price": 268.35
  },
  {
    "id": "9",
    "image":
    "https://m.media-amazon.com/images/I/71dp5f24TbL._AC_UL640_FMwebp_QL65_.jpg",
    "title": "Element Season",
    "price": 552.84
  },
  {
    "id": "10",
    "image":
    "https://m.media-amazon.com/images/I/71Kj-jV5v8L._AC_UL640_FMwebp_QL65_.jpg",
    "title": "Element Vertical",
    "price": 713.90
  },
  {
    "id": "11",
    "image":
    "https://m.media-amazon.com/images/I/71jlppwpjmL._AC_UL640_FMwebp_QL65_.jpg",
    "title": "Element Heavy",
    "price": 1168.75
  },
  {
    "id": "12",
    "image":
    "https://m.media-amazon.com/images/I/71BSdq6OzDL._AC_UL640_FMwebp_QL65_.jpg",
    "title": "Element Grey",
    "price": 947.50
  },
  {
    "id": "13",
    "image":
    "https://m.media-amazon.com/images/I/81RAeKF-8wL._AC_UL640_FMwebp_QL65_.jpg",
    "title": "Element",
    "price": 864.94
  },
  {
    "id": "14",
    "image":
    "https://m.media-amazon.com/images/I/717tHbEHDnL._AC_UL640_FMwebp_QL65_.jpg",
    "title": "Element Signature",
    "price": 829.84
  },
  {
    "id": "15",
    "image":
    "https://m.media-amazon.com/images/I/81rOs3LA0LL._AC_UL640_FMwebp_QL65_.jpg",
    "title": "Element Section",
    "price": 899.00
  },
  {
    "id": "16",
    "image":
    "https://m.media-amazon.com/images/I/61-xQZORAKL._AC_UL640_FMwebp_QL65_.jpg",
    "title": "Element hombre",
    "price": 827.06
  },
  {
    "id": "17",
    "image":
    "https://m.media-amazon.com/images/I/71RUdoglJML._AC_UL640_FMwebp_QL65_.jpg",
    "title": "Element Light",
    "price": 886.52
  },
  {
    "id": "18",
    "image":
    "https://m.media-amazon.com/images/I/714tTmj4KvL._AC_UL640_FMwebp_QL65_.jpg",
    "title": "Element Negro",
    "price": 973.50
  }
]''';

class ProductInMemoryRepository implements ProductRepository {
  @override
  Future<List<Product>> get() async {
    return Future.delayed(
        const Duration(seconds: 2), () => _parse(jsonDecode(products)));
  }

  List<Product> _parse(List<dynamic> json) {
    return json.map((jsonItem) => _parseProducts(jsonItem)).toList();
  }

  Product _parseProducts(Map<String, dynamic> json) {
    return Product(json['id'], json['image'], json['title'], json['price']);
  }
}
