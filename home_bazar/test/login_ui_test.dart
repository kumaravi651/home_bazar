import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:home_bazar/src/presentation/login/login_page.dart';

void main() {
  testWidgets('Login Screen UI Testing', (WidgetTester tester) async {
    //find all widgets needed
    final emailField = find.byKey(const ValueKey('EmailField'));
    final passwordField = find.byKey(const ValueKey('PasswordField'));
    final addButton = find.byKey(const ValueKey('SignIn'));

    //execute the actual test
    await tester.pumpWidget(MaterialApp(home: LoginPage()));
    await tester.enterText(emailField, 'Email Testing');
    await tester.enterText(passwordField, 'Password Testing');
    await tester.tap(addButton);
    await tester.pump(); //rebuilds your widget

    //check outputs
    expect(find.text('Email Testing'), findsOneWidget);
    expect(find.text('Password Testing'), findsOneWidget);
  });
}
